export interface ResetPasswordBodyPut {
  password: string;
  token: string;
}
