export interface SignInPostBody {
  email: string;
  password: string;
  csrfToken: string;
}
