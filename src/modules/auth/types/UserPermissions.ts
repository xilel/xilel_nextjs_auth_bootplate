import { UserPermissionEnum } from "@prisma/client";

export type UserPermissions = UserPermissionEnum;
