import { useFormik } from "formik";
import Link from "next/link";
import { useRouter } from "next/router";
import { signIn } from "next-auth/react";
import { useSnackbar } from "notistack";
import { Button } from "primereact/button";
import { Card } from "primereact/card";
import { InputText } from "primereact/inputtext";
import { Message } from "primereact/message";
import { Password } from "primereact/password";
import { useState } from "react";

import { useLogger } from "@/infrastructure/hooks/useLogger";

interface FormValues {
  email: string;
  password: string;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface SignInComponentProps {}

export const SignInComponent: React.FC<SignInComponentProps> = () => {
  const { enqueueSnackbar } = useSnackbar();
  const router = useRouter();
  const logger = useLogger();
  const [globalError, setGlobalError] = useState("");
  const formik = useFormik<FormValues>({
    initialValues: {
      email: "",
      password: "",
    },
    onSubmit: async (values) => {
      try {
        const result = await signIn("credentials-main", {
          email: values.email,
          password: values.password,
          redirect: false,
        });

        if (result?.status === 401) {
          setGlobalError("Wrong login or password");
          return;
        }
        enqueueSnackbar("You have sign in", { variant: "success" });
        router.push("/");
      } catch (error) {
        if (error instanceof Error) {
          setGlobalError("Unknown error");
          logger.error(error?.message ?? "");
        }
      }
    },
    validate: (values) => {
      const errors: any = {};

      if (values.email.length < 1) {
        errors.email = "Required";
      }

      if (values.password.length < 1) {
        errors.password = "Required";
      }

      return errors;
    },
  });

  const isFormFieldInvalid = (name: keyof FormValues) =>
    !!(formik.touched[name] && formik.errors[name]);

  const getFormErrorMessage = (name: keyof FormValues) =>
    isFormFieldInvalid(name) ? (
      <small className="p-error">{formik.errors[name]}</small>
    ) : (
      <small className="p-error">&nbsp;</small>
    );

  return (
    <Card>
      <h2>Sign in to project</h2>
      <form
        onSubmit={formik.handleSubmit}
        className="flex flex-column gap-2"
        style={{ maxWidth: "700px" }}
      >
        <Message
          severity="error"
          text={globalError}
          style={{ display: globalError !== "" ? "inline-flex" : "none" }}
        />

        <div className="flex flex-column gap-2">
          <label htmlFor="email">E-mail or login</label>
          <InputText
            id="email"
            name="email"
            aria-describedby="email-help"
            value={formik.values.email}
            onChange={(e) => {
              formik.setFieldValue("email", e.target.value);
            }}
          />
          {getFormErrorMessage("email")}
        </div>
        <div className="flex flex-column gap-2">
          <label htmlFor="password">Password</label>
          <Password
            id="password"
            name="password"
            value={formik.values.password}
            feedback={false}
            inputClassName="flex-grow-1"
            onChange={(e) => {
              formik.setFieldValue("password", e.target.value);
            }}
          />
          {getFormErrorMessage("password")}
        </div>
        <div className="flex flex-column gap-2">
          <Button type="submit" label="Log in" />
        </div>
      </form>
      <h5>
        <Link href={"/auth/reset-password"}>
          Forgot password? Click to restore password.
        </Link>
      </h5>
      <Link href={"/auth/signup"} passHref>
        <Button>Don{"'"}t have account? Sign up now!</Button>
      </Link>
    </Card>
  );
};
