import axios, { AxiosError } from "axios";
import { useFormik } from "formik";
import { useSnackbar } from "notistack";
import { Button } from "primereact/button";
import { Card } from "primereact/card";
import { InputText } from "primereact/inputtext";
import { Message } from "primereact/message";
import { useState } from "react";

import { useLogger } from "@/infrastructure/hooks/useLogger";
import { ResetPasswordBodyPost } from "@/modules/auth/apiBody/ResetPasswordBodyPost";

interface FormValues {
  email: string;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface SignUpComponentProps {}

export const ResetPasswordRequestForm: React.FC<SignUpComponentProps> = () => {
  const { enqueueSnackbar } = useSnackbar();
  const [globalError, setGlobalError] = useState("");
  const logger = useLogger();
  const formik = useFormik<FormValues>({
    initialValues: {
      email: "",
    },
    onSubmit: async (values) => {
      const body: ResetPasswordBodyPost = {
        email: values.email,
      };

      try {
        const result = await axios.post("/api/auth/reset-password", body);
        if (result.status === 200) {
          enqueueSnackbar(
            "We have send link to reset password on your email.",
            { variant: "success" }
          );
        } else {
          setGlobalError(result.data.message);
        }
      } catch (error) {
        if (error instanceof AxiosError) {
          const errorMessage = error.response?.data.message ?? "unknown error";
          setGlobalError(errorMessage);
          logger.error(error.message);
        }
      }
    },
    validate: (values) => {
      const errors: any = {};

      if (!values.email) {
        errors.email = "Required";
      } else if (
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
      ) {
        errors.email = "Invalid email address";
      }

      return errors;
    },
  });

  const isFormFieldInvalid = (name: keyof FormValues) =>
    !!(formik.touched[name] && formik.errors[name]);

  const getFormErrorMessage = (name: keyof FormValues) =>
    isFormFieldInvalid(name) ? (
      <small className="p-error">{formik.errors[name]}</small>
    ) : (
      <small className="p-error">&nbsp;</small>
    );

  return (
    <Card>
      <h3>Please enter your email to send the password:</h3>
      <form
        onSubmit={formik.handleSubmit}
        className="flex flex-column gap-3"
        style={{ maxWidth: "700px" }}
      >
        <Message
          severity="error"
          text={globalError}
          style={{ display: globalError !== "" ? "inline-flex" : "none" }}
        />

        <div className="flex flex-column gap-2">
          <label htmlFor="email">E-mail</label>
          <InputText
            id="email"
            name="email"
            aria-describedby="email-help"
            value={formik.values.email}
            onChange={(e) => {
              formik.setFieldValue("email", e.target.value);
            }}
          />
          <small id="email-help">Enter your email</small>
          {getFormErrorMessage("email")}
        </div>
        <div className="flex flex-column gap-2">
          <Button type="submit" label="Reset password" />
        </div>
      </form>
    </Card>
  );
};
