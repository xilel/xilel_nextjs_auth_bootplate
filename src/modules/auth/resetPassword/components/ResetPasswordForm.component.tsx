import axios, { AxiosError } from "axios";
import { useFormik } from "formik";
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";
import { Button } from "primereact/button";
import { Card } from "primereact/card";
import { Message } from "primereact/message";
import { Password } from "primereact/password";
import { useState } from "react";

import { ResetPasswordBodyPut } from "@/modules/auth/apiBody/ResetPasswordBodyPut";

interface FormValues {
  password: string;
  confirmPassword: string;
  token: string;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface ResetPasswordFormProps {
  token: string;
}

export const ResetPasswordForm: React.FC<ResetPasswordFormProps> = ({
  token,
}) => {
  const { enqueueSnackbar } = useSnackbar();
  const router = useRouter();
  const [globalError, setGlobalError] = useState("");
  const formik = useFormik<FormValues>({
    initialValues: {
      password: "",
      confirmPassword: "",
      token,
    },
    onSubmit: async (values) => {
      const body: ResetPasswordBodyPut = {
        password: values.password,
        token: values.token,
      };

      try {
        const result = await axios.put("/api/auth/reset-password", body);
        enqueueSnackbar("You have successfully changed password!", {
          variant: "success",
        });
        if (result.status === 201) {
          router.push("/auth/login");
        }
      } catch (error) {
        if (error instanceof AxiosError) {
          const errorMessage = error.response?.data.message ?? "unknown error";
          setGlobalError(errorMessage);
        }
      }
    },
    validate: (values) => {
      const errors: any = {};

      if (values.password !== values.confirmPassword) {
        errors.password = "Password does not much";
      }

      if (values.password.length < 6 || values.password.length > 16) {
        errors.password =
          "Password mush have at least 6 characters and no more then 16 ";
      }

      return errors;
    },
  });

  const isFormFieldInvalid = (name: keyof FormValues) =>
    !!(formik.touched[name] && formik.errors[name]);

  const getFormErrorMessage = (name: keyof FormValues) =>
    isFormFieldInvalid(name) ? (
      <small className="p-error">{formik.errors[name]}</small>
    ) : (
      <small className="p-error">&nbsp;</small>
    );

  return (
    <Card>
      <h3>Please enter new password:</h3>
      <form
        onSubmit={formik.handleSubmit}
        className="flex flex-column gap-3"
        style={{ maxWidth: "700px" }}
      >
        <Message
          severity="error"
          text={globalError}
          style={{ display: globalError !== "" ? "inline-flex" : "none" }}
        />

        <div className="flex flex-column gap-2">
          <input name="token" value={formik.values.token} type="hidden" />
          <label htmlFor="password">Password</label>
          <Password
            id="password"
            name="password"
            value={formik.values.password}
            inputClassName="flex-grow-1"
            onChange={(e) => {
              formik.setFieldValue("password", e.target.value);
            }}
          />
          {getFormErrorMessage("password")}
        </div>
        <div className="flex flex-column gap-2 ">
          <label htmlFor="confirmPassword">Confirm password</label>
          <Password
            id="confirmPassword"
            name="confirmPassword"
            inputClassName="flex-grow-1"
            value={formik.values.confirmPassword}
            onChange={(e) => {
              formik.setFieldValue("confirmPassword", e.target.value);
            }}
          />
          {getFormErrorMessage("confirmPassword")}
        </div>
        <div className="flex flex-column gap-2">
          <Button type="submit" label="Change password" />
        </div>
      </form>
    </Card>
  );
};
