import axios, { AxiosError } from "axios";
import { useFormik } from "formik";
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";
import { Button } from "primereact/button";
import { Card } from "primereact/card";
import { InputText } from "primereact/inputtext";
import { Message } from "primereact/message";
import { Password } from "primereact/password";
import { useState } from "react";

import { useLogger } from "@/infrastructure/hooks/useLogger";
import { SignUpPostBody } from "@/modules/auth/apiBody/SignUpPostBody";

interface FormValues {
  email: "";
  password: "";
  confirmPassword: "";
  username: "";
  name: "";
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface SignUpComponentProps {}

export const SignUpComponent: React.FC<SignUpComponentProps> = () => {
  const { enqueueSnackbar } = useSnackbar();
  const logger = useLogger();
  const router = useRouter();
  const [globalError, setGlobalError] = useState("");
  const formik = useFormik<FormValues>({
    initialValues: {
      email: "",
      password: "",
      confirmPassword: "",
      username: "",
      name: "",
    },
    onSubmit: async (values) => {
      const body: SignUpPostBody = {
        email: values.email,
        name: values.name,
        password: values.password,
        username: values.username,
      };

      try {
        const result = await axios.post("/api/auth/signup", body);
        if (result.status === 201) {
          router.push("/auth/signin");
          enqueueSnackbar(
            "We have created new account. You can now login to it.",
            { variant: "success" }
          );
        } else {
          setGlobalError(result.data.message);
        }
      } catch (error) {
        if (error instanceof AxiosError) {
          const newLocal = error.response?.data?.message ?? "unknown error";
          setGlobalError(newLocal);
          return;
        }
        if (error instanceof Error) {
          logger.error(error.message);
        }
      }
    },
    validate: (values) => {
      const errors: any = {};

      if (!values.email) {
        errors.email = "Required";
      } else if (
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
      ) {
        errors.email = "Invalid email address";
      }

      if (values.password !== values.confirmPassword) {
        errors.password = "Password does not much";
      }

      if (values.password.length < 6 || values.password.length > 16) {
        errors.password =
          "Password mush have at least 6 characters and no more then 16 ";
      }

      if (values.username.length < 3) {
        errors.username = "Username must have at least 3 characters";
      }

      return errors;
    },
  });

  const isFormFieldInvalid = (name: keyof FormValues) =>
    !!(formik.touched[name] && formik.errors[name]);

  const getFormErrorMessage = (name: keyof FormValues) =>
    isFormFieldInvalid(name) ? (
      <small className="p-error">{formik.errors[name]}</small>
    ) : (
      <small className="p-error">&nbsp;</small>
    );

  return (
    <Card>
      <h3>Field info, to registrate in the system.</h3>
      <form
        onSubmit={formik.handleSubmit}
        className="flex flex-column gap-3"
        style={{ maxWidth: "700px" }}
      >
        <Message
          severity="error"
          text={globalError}
          style={{ display: globalError !== "" ? "inline-flex" : "none" }}
        />
        <div className="flex flex-column gap-2">
          <label htmlFor="username">Username</label>
          <InputText
            id="username"
            name="username"
            aria-describedby="username-help"
            value={formik.values.username}
            onChange={(e) => {
              formik.setFieldValue("username", e.target.value);
            }}
          />
          <small id="username-help">Enter your username, to log in</small>
          {getFormErrorMessage("username")}
        </div>
        <div className="flex flex-column gap-2">
          <label htmlFor="name">Visible name</label>
          <InputText
            id="name"
            name="name"
            aria-describedby="name-help"
            value={formik.values.name}
            onChange={(e) => {
              formik.setFieldValue("name", e.target.value);
            }}
          />
          <small id="name-help">Enter your name</small>
          {getFormErrorMessage("name")}
        </div>
        <div className="flex flex-column gap-2">
          <label htmlFor="email">E-mail</label>
          <InputText
            id="email"
            name="email"
            aria-describedby="email-help"
            value={formik.values.email}
            onChange={(e) => {
              formik.setFieldValue("email", e.target.value);
            }}
          />
          <small id="email-help">Enter your email</small>
          {getFormErrorMessage("email")}
        </div>
        <div className="flex flex-column gap-2">
          <label htmlFor="password">Password</label>
          <Password
            id="password"
            name="password"
            value={formik.values.password}
            inputClassName="flex-grow-1"
            onChange={(e) => {
              formik.setFieldValue("password", e.target.value);
            }}
          />
          {getFormErrorMessage("password")}
        </div>
        <div className="flex flex-column gap-2 ">
          <label htmlFor="confirmPassword">Confirm password</label>
          <Password
            id="confirmPassword"
            name="confirmPassword"
            inputClassName="flex-grow-1"
            value={formik.values.confirmPassword}
            onChange={(e) => {
              formik.setFieldValue("confirmPassword", e.target.value);
            }}
          />
          {getFormErrorMessage("confirmPassword")}
        </div>
        <div className="flex flex-column gap-2">
          <Button type="submit" label="Submit" />
        </div>
      </form>
    </Card>
  );
};
