import { Button } from "@react-email/button";
import { Html } from "@react-email/html";

interface ResetPasswordProps {
  url: string;
}

export const ResetPasswordEmail: React.FC<ResetPasswordProps> = ({ url }) => (
  <Html>
    <h1>Follow the following link</h1>
    <p>
      Please follow
      <Button href={url}>Go to the site</Button>
      to reset your password
    </p>
  </Html>
);
