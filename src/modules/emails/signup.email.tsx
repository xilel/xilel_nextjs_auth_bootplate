import { Button } from "@react-email/button";
import { Html } from "@react-email/html";

interface SignUpEmailProps {
  email: string;
  username: string;
  url: string;
}

export const SignUpEmail: React.FC<SignUpEmailProps> = ({ url, username }) => (
  <Html>
    <h1>You have sign up to our project!</h1>
    <p>Your username: {username}</p>
    <Button href={url}>Go to the site</Button>
  </Html>
);
