import axios, { AxiosResponse } from "axios";
import { Button } from "primereact/button";
import { Column } from "primereact/column";
import { DataTable } from "primereact/datatable";
import { useState } from "react";
import useSWR from "swr";

import { FlexDiv } from "@/infrastructure/layouts/FlexDiv/FlexDiv.component";

import { ViewUser } from "../types/ViewUser";
import { UserAddForm } from "../UserAddForm";
import { UserChangePermissionsForm } from "../UserChangePermisions";
import { UserDeleteForm } from "../UserDeleteForm";
import { UserEditForm } from "../UserEditForm";

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface AdminUserTableProps {}

export const AdminUserTable: React.FC<AdminUserTableProps> = () => {
  const [isOpenUserAddDialog, setOpenUserAddDialog] = useState(false);
  const [isOpenUserEditDialog, setOpenUserEditDialog] = useState(false);
  const [editDialogUser, setEditDialogUser] = useState<ViewUser | undefined>();
  const [isOpenUserPermisionEditDialog, setOpenUserPermisionDialog] =
    useState(false);
  const [editDialogUserPermision, setEditDialogUserPermision] = useState<
    ViewUser | undefined
  >();
  const [isOpenUserDelteDialog, setOpenUserDeleteDialog] = useState(false);
  const [deleteDialogUser, setDeleteDialogUser] = useState<
    ViewUser | undefined
  >();

  const { data, error, isLoading, mutate } = useSWR<AxiosResponse<any>>(
    "/api/admin/user",
    axios
  );

  if (error) return <div>Loading error</div>;
  if (isLoading) return <div>Loading</div>;

  const userList = data?.data?.userList;

  const actionRow = (user: ViewUser) => (
    <FlexDiv gap={2}>
      <Button
        icon="pi pi-user-edit"
        onClick={() => {
          setOpenUserEditDialog(true);
          setEditDialogUser(user);
        }}
      >
        Edit user
      </Button>
      <Button
        icon="pi pi-user-edit"
        onClick={() => {
          setOpenUserPermisionDialog(true);
          setEditDialogUserPermision(user);
        }}
      >
        Change permission
      </Button>
      <Button
        icon="pi pi-times"
        onClick={() => {
          setOpenUserDeleteDialog(true);
          setDeleteDialogUser(user);
        }}
      >
        Delete user
      </Button>
    </FlexDiv>
  );

  const addHeader = () => (
    <FlexDiv gap={2} style={{ width: "100%" }}>
      <span>Actions</span>
    </FlexDiv>
  );

  return (
    <FlexDiv gap={2} direction="column" style={{ width: "100%" }}>
      <Button
        icon="pi pi-user-plus"
        onClick={() => {
          setOpenUserAddDialog(true);
        }}
      >
        Add user
      </Button>

      <UserAddForm
        open={isOpenUserAddDialog}
        onClose={() => {
          setOpenUserAddDialog(false);
          mutate();
        }}
      />
      <UserEditForm
        user={editDialogUser}
        open={isOpenUserEditDialog}
        onClose={() => {
          setOpenUserEditDialog(false);
          setEditDialogUser(undefined);
          mutate();
        }}
      />
      <UserChangePermissionsForm
        user={editDialogUserPermision}
        open={isOpenUserPermisionEditDialog}
        onClose={() => {
          setOpenUserPermisionDialog(false);
          setEditDialogUserPermision(undefined);
          mutate();
        }}
      />
      <UserDeleteForm
        user={deleteDialogUser}
        open={isOpenUserDelteDialog}
        onClose={() => {
          setOpenUserDeleteDialog(false);
          setDeleteDialogUser(undefined);
          mutate();
        }}
      />

      <DataTable value={userList} tableStyle={{ width: "100%" }}>
        <Column field="userName" header="Login" />
        <Column field="name" header="Visible name" />
        <Column field="email" header="E-mail" />
        <Column header={addHeader} body={actionRow} />
      </DataTable>
    </FlexDiv>
  );
};
