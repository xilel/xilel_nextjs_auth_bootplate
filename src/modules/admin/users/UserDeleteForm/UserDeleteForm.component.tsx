import axios, { AxiosError } from "axios";
import { useFormik } from "formik";
import { enqueueSnackbar } from "notistack";
import { Button } from "primereact/button";
import { Dialog } from "primereact/dialog";
import { Message } from "primereact/message";
import { useState } from "react";

import { useLogger } from "@/infrastructure/hooks/useLogger";

import { ViewUser } from "../types/ViewUser";

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface UserDeleteFormProps {
  user: ViewUser | undefined;
  open?: boolean;
  onClose?: () => void;
}

export const UserDeleteForm: React.FC<UserDeleteFormProps> = ({
  user,
  open = false,
  onClose = () => {},
}) => {
  const logger = useLogger();

  const [globalError, setGlobalError] = useState("");
  const formik = useFormik({
    initialValues: {},
    onSubmit: async () => {
      const body = {};

      try {
        const result = await axios.delete(
          `/api/admin/user/${user?.id ?? 0}`,
          body
        );
        if (result.status === 200) {
          enqueueSnackbar(`Delete user ${user?.userName}`, {
            variant: "success",
          });
          onClose();
        } else {
          setGlobalError(result.data.message);
        }
      } catch (error) {
        if (error instanceof AxiosError) {
          const newLocal = error.response?.data?.message ?? "unknown error";
          setGlobalError(newLocal);
          return;
        }
        if (error instanceof Error) {
          logger.error(error.message);
        }
      }
    },
    validate: () => {
      const errors: any = {};

      return errors;
    },
  });

  return (
    <Dialog
      onHide={onClose}
      visible={open}
      header={`Delete user ${user?.userName}` ?? ""}
    >
      <form
        onSubmit={formik.handleSubmit}
        className="flex flex-column gap-3"
        style={{ maxWidth: "700px" }}
      >
        <Message
          severity="error"
          text={globalError}
          style={{ display: globalError !== "" ? "inline-flex" : "none" }}
        />
        <p>Are you sure what you want to delete user?</p>
        <div className="flex flex-row gap-2 ">
          <Button
            type="submit"
            label="Delete user"
            className="flex-grow-1 p-button-danger"
          />
          <Button
            label="Cancel"
            onClick={() => {
              onClose();
            }}
          />
        </div>
      </form>
    </Dialog>
  );
};
