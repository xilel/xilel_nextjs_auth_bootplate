import axios, { AxiosError } from "axios";
import { useFormik } from "formik";
import { enqueueSnackbar } from "notistack";
import { Button } from "primereact/button";
import { Dialog } from "primereact/dialog";
import { InputText } from "primereact/inputtext";
import { Message } from "primereact/message";
import { Password } from "primereact/password";
import { useEffect, useState } from "react";

import { useLogger } from "@/infrastructure/hooks/useLogger";
import { AdminUserPostRequest } from "@/pages/api/admin/user";
import { AdminUserPutRequest } from "@/pages/api/admin/user/[id]";

import { ViewUser } from "../types/ViewUser";

type FormValues = AdminUserPutRequest;

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface UserEditFormProps {
  user: ViewUser | undefined;
  open?: boolean;
  onClose?: () => void;
}

export const UserEditForm: React.FC<UserEditFormProps> = ({
  user,
  open = false,
  onClose = () => {},
}) => {
  const logger = useLogger();

  const [globalError, setGlobalError] = useState("");
  const formik = useFormik<FormValues>({
    initialValues: {
      email: user?.email ?? "",
      password: "",
      userName: user?.userName ?? "",
      name: user?.userName ?? "",
    },
    onSubmit: async (values) => {
      const body: AdminUserPostRequest = {
        email: values.email,
        name: values.name,
        password: values.password,
        userName: values.userName,
      };

      try {
        const result = await axios.put(
          `/api/admin/user/${user?.id ?? 0}`,
          body
        );
        if (result.status === 200) {
          enqueueSnackbar("Edited new user", { variant: "success" });
          onClose();
        } else {
          setGlobalError(result.data.message);
        }
      } catch (error) {
        if (error instanceof AxiosError) {
          const newLocal = error.response?.data?.message ?? "unknown error";
          setGlobalError(newLocal);
          return;
        }
        if (error instanceof Error) {
          logger.error(error.message);
        }
      }
    },
    validate: (values) => {
      const errors: any = {};

      if (!values.email) {
        errors.email = "Required";
      } else if (
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
      ) {
        errors.email = "Invalid email address";
      }

      if (values.password.length < 6 || values.password.length > 16) {
        errors.password =
          "Password mush have at least 6 characters and no more then 16 ";
      }

      if (values.userName.length < 3) {
        errors.username = "Username must have at least 3 characters";
      }

      return errors;
    },
  });

  useEffect(() => {
    if (open === true) {
      formik.setValues({
        email: user?.email ?? "",
        password: "",
        userName: user?.userName ?? "",
        name: user?.userName ?? "",
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [open, user]);

  const isFormFieldInvalid = (name: keyof FormValues) =>
    !!(formik.touched[name] && formik.errors[name]);

  const getFormErrorMessage = (name: keyof FormValues) =>
    isFormFieldInvalid(name) ? (
      <small className="p-error">{formik.errors[name]}</small>
    ) : (
      <small className="p-error">&nbsp;</small>
    );

  return (
    <Dialog
      onHide={onClose}
      visible={open}
      header={`Edit user ${user?.userName}` ?? ""}
    >
      <form
        onSubmit={formik.handleSubmit}
        className="flex flex-column gap-3"
        style={{ maxWidth: "700px" }}
      >
        <Message
          severity="error"
          text={globalError}
          style={{ display: globalError !== "" ? "inline-flex" : "none" }}
        />
        <div className="flex flex-column gap-2">
          <label htmlFor="username">Username</label>
          <InputText
            id="userName"
            name="userName"
            aria-describedby="userName-help"
            value={formik.values.userName}
            onChange={(e) => {
              formik.setFieldValue("userName", e.target.value);
            }}
          />
          <small id="userName-help">Enter your username, to log in</small>
          {getFormErrorMessage("userName")}
        </div>
        <div className="flex flex-column gap-2">
          <label htmlFor="name">Visible name</label>
          <InputText
            id="name"
            name="name"
            aria-describedby="name-help"
            value={formik.values.name}
            onChange={(e) => {
              formik.setFieldValue("name", e.target.value);
            }}
          />
          <small id="name-help">Enter your name</small>
          {getFormErrorMessage("name")}
        </div>
        <div className="flex flex-column gap-2">
          <label htmlFor="email">E-mail</label>
          <InputText
            id="email"
            name="email"
            aria-describedby="email-help"
            value={formik.values.email}
            onChange={(e) => {
              formik.setFieldValue("email", e.target.value);
            }}
          />
          <small id="email-help">Enter your email</small>
          {getFormErrorMessage("email")}
        </div>
        <div className="flex flex-column gap-2">
          <label htmlFor="password">Password</label>
          <Password
            id="password"
            name="password"
            value={formik.values.password}
            inputClassName="flex-grow-1"
            onChange={(e) => {
              formik.setFieldValue("password", e.target.value);
            }}
          />
          {getFormErrorMessage("password")}
        </div>
        <div className="flex flex-column gap-2">
          <Button type="submit" label="Edit user" />
        </div>
      </form>
    </Dialog>
  );
};
