import { UserPermissions } from "@/modules/auth/types/UserPermissions";

export interface ViewUser {
  id: number;
  userName: string;
  email: string;
  name: string;
  pictureUrl: string;
  permissionList: UserPermissions[];
}
