import axios, { AxiosError } from "axios";
import { useFormik } from "formik";
import { useSnackbar } from "notistack";
import { Button } from "primereact/button";
import { Checkbox } from "primereact/checkbox";
import { Dialog } from "primereact/dialog";
import { Message } from "primereact/message";
import { useEffect, useState } from "react";
import { useMap } from "usehooks-ts";

import { useLogger } from "@/infrastructure/hooks/useLogger";
import { FlexDiv } from "@/infrastructure/layouts/FlexDiv/FlexDiv.component";
import { AdminUserChangePermissionPutRequest } from "@/pages/api/admin/user/change-permision";

import { ViewUser } from "../types/ViewUser";

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface UserChangePermissionsFormProps {
  user?: ViewUser;
  open: boolean;
  onClose: () => void;
}

export const UserChangePermissionsForm: React.FC<
  UserChangePermissionsFormProps
> = ({ onClose, open, user }) => {
  const logger = useLogger();
  const { enqueueSnackbar } = useSnackbar();
  const [permissionMap, permissionMapAction] = useMap(
    new Map(user?.permissionList.map((x) => [x, x])) ?? []
  );
  const [globalError, setGlobalError] = useState("");
  useEffect(() => {
    if (open === true) {
      permissionMapAction.reset();
      user?.permissionList.map((x) => permissionMapAction.set(x, x));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [open, user?.permissionList]);
  const formik = useFormik({
    initialValues: {},
    onSubmit: async () => {
      const body: AdminUserChangePermissionPutRequest = {
        userId: user?.id ?? 0,
        permissions: [...permissionMap.keys()],
      };
      try {
        const result = await axios.put(
          "/api/admin/user/change-permision",
          body
        );
        if (result.status === 200) {
          enqueueSnackbar("Changed user permission", { variant: "success" });
          onClose();
        } else {
          setGlobalError(result.data.message);
        }
      } catch (error) {
        if (error instanceof AxiosError) {
          const newLocal = error.response?.data?.message ?? "unknown error";
          setGlobalError(newLocal);
          return;
        }
        if (error instanceof Error) {
          logger.error(error.message);
        }
      }
    },
    validate: () => ({}),
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <Dialog
        onHide={onClose}
        visible={open}
        header="Change user permissions"
        footer={
          <Button
            label="Save new permission"
            onClick={() => {
              formik.submitForm();
            }}
          />
        }
      >
        <Message
          severity="error"
          text={globalError}
          style={{ display: globalError !== "" ? "inline-flex" : "none" }}
        />
        <FlexDiv direction="column" gap={2}>
          <FlexDiv gap={1}>
            <Checkbox
              checked={permissionMap.has("SUPERUSER")}
              onChange={(e) => {
                if (e.checked) {
                  permissionMapAction.set("SUPERUSER", "SUPERUSER");
                } else {
                  permissionMapAction.remove("SUPERUSER");
                }
              }}
            />
            <label>Super user. Give all permissons.</label>
          </FlexDiv>
          <h3>Edit user group</h3>
          <FlexDiv direction="row" wrap="wrap" gap={5}>
            <FlexDiv gap={1}>
              <Checkbox
                checked={permissionMap.has("see_user_list")}
                onChange={(e) => {
                  if (e.checked) {
                    permissionMapAction.set("see_user_list", "see_user_list");
                  } else {
                    permissionMapAction.remove("see_user_list");
                  }
                }}
              />
              <label>See user table</label>
            </FlexDiv>
            <FlexDiv gap={1}>
              <Checkbox
                checked={permissionMap.has("add_user")}
                onChange={(e) => {
                  if (e.checked) {
                    permissionMapAction.set("add_user", "add_user");
                  } else {
                    permissionMapAction.remove("add_user");
                  }
                }}
              />
              <label>Create new user</label>
            </FlexDiv>
            <FlexDiv gap={1}>
              <Checkbox
                checked={permissionMap.has("edit_user")}
                onChange={(e) => {
                  if (e.checked) {
                    permissionMapAction.set("edit_user", "edit_user");
                  } else {
                    permissionMapAction.remove("edit_user");
                  }
                }}
              />
              <label>Edit user</label>
            </FlexDiv>
            <FlexDiv gap={1}>
              <Checkbox
                checked={permissionMap.has("delete_user")}
                onChange={(e) => {
                  if (e.checked) {
                    permissionMapAction.set("delete_user", "delete_user");
                  } else {
                    permissionMapAction.remove("delete_user");
                  }
                }}
              />
              <label>Delete user</label>
            </FlexDiv>
          </FlexDiv>
        </FlexDiv>
      </Dialog>
    </form>
  );
};
