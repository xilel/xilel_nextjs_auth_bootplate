import { useRouter } from "next/router";
import { Button } from "primereact/button";
import { Card } from "primereact/card";

import { CenterLayout } from "@/infrastructure/layouts/CenterLayout/CenterLayout.component";

// eslint-disable-next-line @typescript-eslint/no-empty-interface

export default function AccessDeniedPage() {
  const router = useRouter();

  return (
    <CenterLayout>
      <Card style={{ textAlign: "center" }}>
        <h1>Access denied</h1>
        <p>You do not have permission to use this page</p>

        <Button
          onClick={() => {
            router.back();
          }}
        >
          Go back
        </Button>
      </Card>
    </CenterLayout>
  );
}
