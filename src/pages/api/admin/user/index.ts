import { genSalt, hash } from "bcrypt";
import { NextApiRequest, NextApiResponse } from "next";

import { checkUserPermissionInApi } from "@/infrastructure/lib/checkUserPermission";
import prisma from "@/infrastructure/lib/prisma";
import { ViewUser } from "@/modules/admin/users/types/ViewUser";

interface AdminUserGetResponce {
  userList: ViewUser[];
}

export interface AdminUserPostRequest {
  userName: string;
  email: string;
  name: string;
  password: string;
}

async function GET(req: NextApiRequest, res: NextApiResponse) {
  const permissionCheck = await checkUserPermissionInApi(req, "see_user_list");

  if (permissionCheck === "denied") {
    res.status(403).json({
      success: false,
      message: "Permission denied!",
    });
    return;
  }

  const rawUser = await prisma.user.findMany({
    orderBy: { id: "desc" },
    include: { userPermision: true },
  });
  const userList: ViewUser[] = rawUser.map((x) => ({
    id: x.id,
    email: x.email ?? "",
    name: x.name ?? "",
    pictureUrl: x.image ?? "",
    userName: x.username ?? "",
    permissionList: x.userPermision.map((y) => y.permission),
  }));

  const responceBody: AdminUserGetResponce = {
    userList,
  };
  res.status(200).json(responceBody);
}

async function POST(req: NextApiRequest, res: NextApiResponse) {
  const newUser: AdminUserPostRequest = req.body;
  // Check if user exists

  const permissionCheck = await checkUserPermissionInApi(req, "add_user");

  if (permissionCheck === "denied") {
    res.status(403).json({
      success: false,
      message: "Permission denied!",
    });
    return;
  }

  const userExists = await prisma.user.findFirst({
    where: {
      OR: [
        { username: newUser.userName ?? "" },
        { email: newUser.email ?? "" },
      ],
    },
  });

  if (userExists) {
    if (userExists.email === newUser.email) {
      res.status(422).json({
        success: false,
        message: "A user with the same login or  email already exists!",
        userExists: true,
      });
    } else if (userExists.username === newUser.userName) {
      res.status(422).json({
        success: false,
        message: "A user with the same login already exists!",
        userExists: true,
      });
    }

    return;
  }

  const salt = await genSalt(10);
  const passwordHash = await hash(newUser.password ?? "", salt);

  // Store new user
  await prisma.user.create({
    data: {
      email: newUser.email,
      name: newUser.name,
      password: passwordHash,
      username: newUser.userName,
    },
  });

  res.status(201).json({
    success: true,
    message: "A user was created!",
  });
}

export default async function getViewUserApiRequest(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method === "GET") {
    await GET(req, res);
    return;
  }
  if (req.method === "POST") {
    await POST(req, res);
    return;
  }

  res.status(405).json({ message: "Method unavailable" });
}
