import { genSalt, hash } from "bcrypt";
import { NextApiRequest, NextApiResponse } from "next";

import { checkUserPermissionInApi } from "@/infrastructure/lib/checkUserPermission";
import prisma from "@/infrastructure/lib/prisma";

export interface AdminUserPutRequest {
  userName: string;
  email: string;
  name: string;
  password: string;
}

async function PUT(req: NextApiRequest, res: NextApiResponse) {
  const user = req.body as AdminUserPutRequest;
  const userId = Number(req.query.id);

  const permissionCheck = await checkUserPermissionInApi(req, "edit_user");
  if (permissionCheck === "denied") {
    res.status(403).json({
      success: false,
      message: "Permission denied!",
    });
    return;
  }

  const salt = await genSalt(10);
  const passwordHash = await hash(user.password ?? "", salt);

  try {
    await prisma.user.update({
      where: { id: userId },
      data: {
        username: user.userName,
        name: user.name,
        email: user.email,
        password: user.password !== "" ? passwordHash : undefined,
      },
    });
    res.status(200).json({
      success: true,
      message: "A user was edited!",
    });
  } catch (error) {
    res
      .status(500)
      .json({ sucess: false, message: "Unknown error with server" });
  }
}

async function DELETE(req: NextApiRequest, res: NextApiResponse) {
  const userId = Number(req.query.id);

  const permissionCheck = await checkUserPermissionInApi(req, "delete_user");
  if (permissionCheck === "denied") {
    res.status(403).json({
      success: false,
      message: "Permission denied!",
    });
    return;
  }

  try {
    await prisma.user.delete({ where: { id: userId } });
    res.status(200).json({
      success: true,
      message: "A user was edited!",
    });
  } catch (error) {
    res
      .status(500)
      .json({ sucess: false, message: "Unknown error with server" });
  }
}

export default async function getViewUserApiRequest(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method === "PUT") {
    PUT(req, res);
    return;
  }
  if (req.method === "DELETE") {
    DELETE(req, res);
    return;
  }

  res.status(405).json({ message: "Method unavailable" });
}
