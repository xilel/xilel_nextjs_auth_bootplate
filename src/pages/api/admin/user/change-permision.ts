import { NextApiRequest, NextApiResponse } from "next";

import { checkUserPermissionInApi } from "@/infrastructure/lib/checkUserPermission";
import prisma from "@/infrastructure/lib/prisma";
import { UserPermissions } from "@/modules/auth/types/UserPermissions";

export interface AdminUserChangePermissionPutRequest {
  userId: number;
  permissions: UserPermissions[];
}

async function PUT(req: NextApiRequest, res: NextApiResponse) {
  const { permissions = [], userId = 0 }: AdminUserChangePermissionPutRequest =
    req.body;
  // Check if user exists

  const permissionCheck = await checkUserPermissionInApi(req, "edit_user");

  if (permissionCheck === "denied") {
    res.status(403).json({
      success: false,
      message: "Permission denied!",
    });
    return;
  }

  try {
    await prisma.$transaction([
      prisma.userPermision.deleteMany({ where: { userId } }),
      prisma.userPermision.createMany({
        data: permissions.map((x) => ({ userId, permission: x })),
      }),
    ]);

    res.status(200).json({
      success: true,
      message: "User permissions was updated!",
    });
  } catch (e) {
    res.status(500).json({
      success: false,
      message: "Error on server side!",
    });
    throw e;
  }
}

export default async function getViewUserApiRequest(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method === "PUT") {
    await PUT(req, res);
    return;
  }

  res.status(405).json({ message: "Method unavailable" });
}
