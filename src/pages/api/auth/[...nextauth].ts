import { PrismaAdapter } from "@next-auth/prisma-adapter";
import { compare } from "bcrypt";
import { NextApiHandler } from "next";
import NextAuth, { NextAuthOptions } from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";

import prisma from "@/infrastructure/lib/prisma";

if (!process.env.NEXTAUTH_SECRET) {
  throw new Error("Please provide process.env.NEXTAUTH_SECRET");
}

// https://reacthustle.com/blog/nextjs-setup-role-based-authentication

const options: NextAuthOptions = {
  providers: [
    CredentialsProvider({
      id: "credentials-main",
      name: "Credentials Main",
      type: "credentials",
      credentials: {
        email: { label: "E-mail", type: "text" },
        password: { label: "Password", type: "password" },
      },
      authorize: async (credentials) => {
        if (credentials === undefined) {
          return null;
        }

        const user = await prisma.user.findFirst({
          where: {
            OR: [{ email: credentials.email }, { username: credentials.email }],
          },
          include: { userPermision: true },
        });

        if (
          user === undefined ||
          user === null ||
          user.password === undefined ||
          user.password === null
        ) {
          return null;
        }

        const isPasswordPass = await compare(
          credentials.password,
          user.password
        );

        if (isPasswordPass === true) {
          const userPermissions = user.userPermision.map((x) => x.permission);

          return {
            ...user,
            id: user.id.toString(),
            permissions: userPermissions,
          };
        }

        return null;
      },
    }),
  ],
  adapter: PrismaAdapter(prisma),
  secret: process.env.NEXTAUTH_SECRET,
  session: {
    strategy: "jwt",
    maxAge: 30 * 24 * 60 * 60, // 30 days,
    updateAge: 60 * 15,
  },
  theme: {
    colorScheme: "dark",
  },
  callbacks: {
    async jwt({ token, user, trigger }) {
      /* Step 1: update the token based on the user object */
      if (user && trigger !== "update") {
        // eslint-disable-next-line no-param-reassign
        token.permissions = user.permissions;
      }

      return token;
    },
    session({ session, token, trigger }) {
      /* Step 2: update the session.user based on the token object */
      if (token && session.user && trigger !== "update") {
        // eslint-disable-next-line no-param-reassign
        session.user.permissions = token.permissions;
      }
      return session;
    },
  },
};

const authHandler: NextApiHandler = (req, res) => NextAuth(req, res, options);
export default authHandler;
