import { PrismaClient } from "@prisma/client";
import { render } from "@react-email/render";
import { genSalt, hash } from "bcrypt";
import { NextApiRequest, NextApiResponse } from "next";
import { v4 as uuidV4 } from "uuid";

import { getEmailTrasporter } from "@/infrastructure/lib/getEmailTransport";
import prisma from "@/infrastructure/lib/prisma";
import { ResetPasswordBodyPost } from "@/modules/auth/apiBody/ResetPasswordBodyPost";
import { ResetPasswordEmail } from "@/modules/emails/resetPassword.email";

export default async function handle(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method === "POST") {
    const { email } = req.body as Partial<ResetPasswordBodyPost>;

    try {
      const db = prisma as PrismaClient;

      // Check for user existence
      const user = await db.user.findFirst({ where: { email } });

      if (!user) {
        res.status(422).json({ message: "User doesn't exists!" });
        return;
      }
      const token = await db.emailPasswordResetToken.findFirst({
        where: { userId: user.id },
      });

      if (token) {
        await db.emailPasswordResetToken.delete({ where: { userId: user.id } });
      }

      // Create a token id
      const securedTokenId = uuidV4();

      // Store token in DB
      await db.emailPasswordResetToken.create({
        data: {
          userId: user.id,
          token: securedTokenId,
        },
      });

      // Link send to user's email for resetting
      const link = `${process.env.NEXTAUTH_URL}/auth/reset-password/${securedTokenId}`;

      const transporter = getEmailTrasporter();
      const emailRender = render(<ResetPasswordEmail url={link} />);

      await transporter.sendMail({
        from: process.env.SMTP_FROM,
        to: user.email ?? "",
        subject: "Reset Password",
        text: "Reset Password Messsage",
        html: emailRender,
      });
    } catch (error: any) {
      res.status(400).send({ message: error.message });
      return;
    }

    // Success
    res.status(200).json({ success: true });
    return;
  }
  if (req.method === "PUT") {
    const { token: tokenValue, password } = req.body;

    const db = prisma as PrismaClient;

    // Get token from DB
    const token = await db.emailPasswordResetToken.findFirst({
      where: { token: tokenValue },
    });

    if (!token) {
      res.status(400).json({
        success: false,
        message: "Invalid or expired password reset token",
      });
      return;
    }

    const salt = await genSalt(10);
    const passwordHash = await hash(password ?? "", salt);

    const user = await db.user.update({
      where: { id: token.userId },
      data: { password: passwordHash },
    });

    const transporter = getEmailTrasporter();

    await transporter.sendMail({
      from: process.env.SMTP_FROM,
      to: user.email ?? "",
      subject: "Password reset successfully",
      html: "Password is successfully reset",
    });

    // Delete token so it won't be used twice
    await db.emailPasswordResetToken.deleteMany({
      where: { userId: user.id },
    });

    res
      .status(200)
      .json({ seccuess: true, message: "Password is reset successfully" });

    return;
  }
  res.status(400).json({ success: false, message: "Bad request" });
}
