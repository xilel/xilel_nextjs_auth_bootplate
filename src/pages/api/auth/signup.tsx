import { PrismaClient } from "@prisma/client";
import { render } from "@react-email/render";
import { genSalt, hash } from "bcrypt";
import { NextApiRequest, NextApiResponse } from "next";
import Mail from "nodemailer/lib/mailer";

import { getEmailTrasporter } from "@/infrastructure/lib/getEmailTransport";
import prisma from "@/infrastructure/lib/prisma";
import { SignUpPostBody } from "@/modules/auth/apiBody/SignUpPostBody";
import { SignUpEmail } from "@/modules/emails/signup.email";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method === "POST") {
    const newUser: Partial<SignUpPostBody> = req.body;

    const db = prisma as PrismaClient;

    // Check if user exists
    const userExists = await db.user.findFirst({
      where: {
        OR: [
          { username: newUser.username ?? "" },
          { email: newUser.email ?? "" },
        ],
      },
    });
    if (userExists) {
      if (userExists.email === newUser.email) {
        res.status(422).json({
          success: false,
          message: "A user with the same email already exists!",
          userExists: true,
        });
      } else if (userExists.username === newUser.username) {
        res.status(422).json({
          success: false,
          message: "A user with the same login already exists!",
          userExists: true,
        });
      }

      return;
    }

    const salt = await genSalt(10);
    const passwordHash = await hash(newUser.password ?? "", salt);

    // Store new user
    await prisma.user.create({
      data: {
        email: newUser.email,
        name: newUser.name,
        password: passwordHash,
        username: newUser.username,
      },
    });

    const emailTransport = getEmailTrasporter();
    const emailHtml = render(
      <SignUpEmail
        email={newUser.email ?? ""}
        url={process.env.NEXTAUTH_URL ?? ""}
        username={newUser.username ?? ""}
      />
    );
    const emailOption: Mail.Options = {
      to: newUser.email ?? "",
      from: process.env.SMTP_FROM,
      subject: "You have sign up to our project!",
      html: emailHtml,
    };

    try {
      await emailTransport.sendMail(emailOption);
    } catch (error) {
      res.status(201).json({
        success: true,
        message: "You was registred, but we failed to send email",
      });
    }

    res
      .status(201)
      .json({ success: true, message: "User signed up successfully" });
  } else {
    res.status(400).json({ success: false, message: "Invalid method" });
  }
}
