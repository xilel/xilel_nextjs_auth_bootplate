import { getBaseLayout } from "@/infrastructure/layouts/BaseLayout";

import { NextPageWithLayout } from "./_app";

export const Home: NextPageWithLayout = () => <p>Hello. This is index page!</p>;

Home.getLayout = getBaseLayout;

export default Home;
