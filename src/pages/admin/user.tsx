import { useRouter } from "next/router";

import { LoadingComponent } from "@/infrastructure/components/LoadingComponent/Loading.component";
import { useCheckUserPermission } from "@/infrastructure/hooks/useCheckUserPermission";
import { getBaseLayout } from "@/infrastructure/layouts/BaseLayout";
import { AdminUserTable } from "@/modules/admin/users/AdminUserTable/AdminUserTable.component";

import { NextPageWithLayout } from "../_app";

const UserAdminPage: NextPageWithLayout = () => {
  const havePermission = useCheckUserPermission("see_user_list");
  const router = useRouter();

  if (havePermission === "denied") {
    router.replace("/403");
    return <></>;
  }

  if (havePermission === "loading") {
    return <LoadingComponent />;
  }

  return <AdminUserTable />;
};

UserAdminPage.getLayout = getBaseLayout;

export default UserAdminPage;
