import { getCenterLayout } from "@/infrastructure/layouts/CenterLayout/CenterLayout.component";
import { SignInComponent } from "@/modules/auth/signIn/SingInForm.component";

import { NextPageWithLayout } from "../_app";

const SignInPage: NextPageWithLayout = () => <SignInComponent />;

SignInPage.getLayout = getCenterLayout;

export default SignInPage;
