import { useRouter } from "next/router";

import { getCenterLayout } from "@/infrastructure/layouts/CenterLayout/CenterLayout.component";
import { ResetPasswordForm } from "@/modules/auth/resetPassword/components/ResetPasswordForm.component";
import { NextPageWithLayout } from "@/pages/_app";

export const ResetPasswordPage: NextPageWithLayout = () => {
  const router = useRouter();
  const { token } = router.query;

  return <ResetPasswordForm token={token as string} />;
};

ResetPasswordPage.getLayout = getCenterLayout;

export default ResetPasswordPage;
