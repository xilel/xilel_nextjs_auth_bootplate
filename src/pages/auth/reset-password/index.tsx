import { getCenterLayout } from "@/infrastructure/layouts/CenterLayout/CenterLayout.component";
import { ResetPasswordRequestForm } from "@/modules/auth/resetPassword/components/ResetPasswordRequestForm.component";
import { NextPageWithLayout } from "@/pages/_app";

export const ResetPassowordRequestPage: NextPageWithLayout = () => (
  <ResetPasswordRequestForm />
);

ResetPassowordRequestPage.getLayout = getCenterLayout;

export default ResetPassowordRequestPage;
