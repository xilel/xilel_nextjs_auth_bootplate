import { getCenterLayout } from "@/infrastructure/layouts/CenterLayout/CenterLayout.component";
import { SignUpComponent } from "@/modules/auth/SignUp/components/SignUpForm.component";

import { NextPageWithLayout } from "../_app";

export const SignUpPage: NextPageWithLayout = () => <SignUpComponent />;

SignUpPage.getLayout = getCenterLayout;

export default SignUpPage;
