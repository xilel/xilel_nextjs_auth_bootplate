/* eslint-disable no-console */
import React, { useMemo } from "react";

import { ILogger } from "./types/ILogger";

export const LoggerContext = React.createContext<ILogger>({
  error: () => {},
  warn: () => {},
  log: () => {},
});

interface LoggerProviderProps {
  children?: React.ReactNode;
}

export const LoggerProvider: React.FC<LoggerProviderProps> = ({ children }) => {
  const logger: ILogger = useMemo(
    () => ({
      error: (message, stack) => {
        console.error(message, stack);
      },
      warn: (message) => {
        console.warn(message);
      },
      log: (message) => {
        console.log(message);
      },
    }),
    []
  );

  return (
    <LoggerContext.Provider value={logger}>{children}</LoggerContext.Provider>
  );
};
