import { NextApiRequest } from "next";
import { getToken } from "next-auth/jwt";

import { UserPermissions } from "@/modules/auth/types/UserPermissions";

export async function checkUserPermissionInApi(
  req: NextApiRequest,
  ...requiredPermisions: UserPermissions[]
): Promise<"denied" | "approved"> {
  const token = await getToken({ req });
  if (!token) {
    return "denied";
  }

  const permissionSet = new Set(token?.permissions ?? []);

  if (permissionSet.has("SUPERUSER")) {
    return "approved";
  }

  const isHavePermission = requiredPermisions.every((x) =>
    permissionSet.has(x)
  );

  return isHavePermission ? "approved" : "denied";
}
