export function getIsSSR(): boolean {
  return typeof window === "undefined";
}
