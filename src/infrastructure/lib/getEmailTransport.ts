import nodemailer, { Transporter } from "nodemailer";

export function getEmailTrasporter(): Transporter {
  const host = process.env.SMTP_HOST ?? "";
  const port = Number(process.env.SMTP_PORT ?? "0");
  const user = process.env.SMTP_USER;
  const pass = process.env.SMTP_PASSWORD;
  const from = process.env.SMTP_FROM;

  const transport = nodemailer.createTransport({
    host,
    port,
    auth: {
      user,
      pass,
    },
    from,
  });

  return transport;
}
