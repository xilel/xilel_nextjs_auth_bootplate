import { useContext } from "react";

import { LoggerContext } from "../LoggerProvider";
import { ILogger } from "../types/ILogger";

export function useLogger(): ILogger {
  const logger = useContext(LoggerContext);
  return logger;
}
