import { useSession } from "next-auth/react";

import { UserPermissions } from "@/modules/auth/types/UserPermissions";

export function useCheckUserPermission(
  ...requiredPermisions: UserPermissions[]
): "loading" | "denied" | "approved" {
  const { data, status } = useSession();
  if (status === "loading") {
    return "loading";
  }
  if (status === "unauthenticated") {
    return "denied";
  }

  const permissionSet = new Set(data?.user?.permissions ?? []);

  if (permissionSet.has("SUPERUSER")) {
    return "approved";
  }

  const isHavePermission = requiredPermisions.every((x) =>
    permissionSet.has(x)
  );

  return isHavePermission ? "approved" : "denied";
}
