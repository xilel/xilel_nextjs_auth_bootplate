/* eslint-disable no-nested-ternary */
import { CustomContentProps, useSnackbar } from "notistack";
import { Message } from "primereact/message";
import React from "react";

type NotistackMessageComponentProps = CustomContentProps;

export const NotistackMessageComponent = React.forwardRef<
  HTMLDivElement,
  NotistackMessageComponentProps
>((props: NotistackMessageComponentProps, ref) => {
  const { closeSnackbar } = useSnackbar();
  const variant =
    props.variant === "error" || props.variant === "success"
      ? props.variant
      : props.variant === "warning"
      ? "warn"
      : "info";

  const forBarColour =
    props.variant === "error"
      ? "red-500"
      : props.variant === "warning"
      ? "yellow-500"
      : props.variant === "success"
      ? "green-500"
      : "blue-500";

  const { autoHideDuration, className } = props;

  return (
    <div
      style={{ position: "relative" }}
      ref={ref}
      className={className}
      onClick={() => {
        closeSnackbar(props.id);
      }}
    >
      <Message
        severity={variant}
        text={props.message}
        style={{ width: "100%" }}
      />
      <div
        style={{
          position: "absolute",
          left: 0,
          bottom: 0,
          width: "100%",
          height: "4px",
          backgroundColor: `var(--${forBarColour})`,
          borderBottomLeftRadius: "3px",
          borderBottomRightRadius: "3px",
          animation: "animate-width",
          animationDuration: `${autoHideDuration ?? 0}ms`,
          animationTimingFunction: "linear",
          animationDirection: "reverse",
          animationFillMode: "forwards",
        }}
      />
    </div>
  );
});

NotistackMessageComponent.displayName = "NotistackMessageComponentName";
