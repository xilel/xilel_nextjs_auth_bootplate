import { ProgressSpinner } from "primereact/progressspinner";

import { FlexDiv } from "@/infrastructure/layouts/FlexDiv/FlexDiv.component";

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface LoadingComponentProps {}

export const LoadingComponent: React.FC<LoadingComponentProps> = () => (
  <FlexDiv
    style={{ height: "100%", width: "100%" }}
    justifyContent="center"
    alignItems="center"
  >
    <ProgressSpinner />
  </FlexDiv>
);
