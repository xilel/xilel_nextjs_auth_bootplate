import { ReactElement } from "react";

interface CenterLayoutProps {
  children?: React.ReactNode;
}

export const CenterLayout: React.FC<CenterLayoutProps> = ({ children }) => (
  <div
    style={{
      height: "100vh",
      width: "100vw",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      position: "absolute",
      left: 0,
      top: 0,
    }}
  >
    {children}
  </div>
);

export function getCenterLayout(page: ReactElement) {
  return <CenterLayout>{page}</CenterLayout>;
}
