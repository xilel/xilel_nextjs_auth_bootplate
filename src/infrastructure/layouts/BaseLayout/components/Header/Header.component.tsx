import Link from "next/link";
import { useRouter } from "next/router";
import { signOut, useSession } from "next-auth/react";
import { Button } from "primereact/button";
import { Menubar } from "primereact/menubar";
import { MenuItem } from "primereact/menuitem";
import { useMemo } from "react";

import { FlexDiv } from "@/infrastructure/layouts/FlexDiv/FlexDiv.component";
import { UserPermissions } from "@/modules/auth/types/UserPermissions";

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface HeaderComponentProps {}

export const HeaderComponent: React.FC<HeaderComponentProps> = () => {
  const router = useRouter();
  const { data: session } = useSession();

  const permissionSet = useMemo(
    () => new Set(session?.user?.permissions ?? []),
    [session?.user?.permissions]
  );

  const isActive: (pathname: string) => boolean = (pathname) =>
    router.pathname === pathname;

  const isHavePermission = (...rest: UserPermissions[]) =>
    permissionSet.has("SUPERUSER") || rest.every((x) => permissionSet.has(x));

  const notLoginElement = (
    <FlexDiv gap={2}>
      <Link href="/auth/signin" legacyBehavior passHref>
        <Button>
          <a data-active={isActive("/signup")}>Log in</a>
        </Button>
      </Link>
    </FlexDiv>
  );

  const loginElement = (
    <FlexDiv gap={2}>
      <Button onClick={() => signOut({ callbackUrl: "/" })}>
        <a>Log out</a>
      </Button>
    </FlexDiv>
  );

  const start = <img alt="logo" src="/logo.png" height="40" className="mr-2" />;

  const end = session !== null ? loginElement : notLoginElement;

  const items: MenuItem[] = [
    {
      label: "Home",
      command: () => {
        router.push("/");
      },
      icon: "pi pi-home",
    },
    {
      label: "Admin",
      visible: isHavePermission("see_user_list"),
      items: [
        {
          label: "User list",
          visible: isHavePermission("see_user_list"),
          command: () => {
            router.push("/admin/user");
          },
        },
      ],
      icon: "pi pi-globe",
    },
  ];
  return <Menubar start={start} model={items} end={end} />;
};
