import { ReactElement } from "react";

import { HeaderComponent } from "./components/Header";

interface BaseLayoutProps {
  children?: React.ReactNode;
}

export const BaseLayout: React.FC<BaseLayoutProps> = ({ children }) => (
  <div>
    <header>
      <HeaderComponent />
    </header>
    <main style={{ padding: "8px" }}>{children}</main>
  </div>
);

export function getBaseLayout(page: ReactElement) {
  return <BaseLayout>{page}</BaseLayout>;
}
