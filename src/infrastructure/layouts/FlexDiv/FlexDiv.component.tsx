interface FlexDivProps extends React.HTMLAttributes<HTMLDivElement> {
  direction?: "row" | "column" | "row-reverse" | "flex-column-reverse";
  wrap?: "wrap" | "wrap-reverse" | "nowrap";
  gap?: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8;
  justifyContent?: "start" | "end" | "center" | "between" | "around" | "evenly";
  alignContent?: "start" | "end" | "center" | "between" | "around" | "evenly";
  alignItems?: "start" | "end" | "stretch" | "center" | "baseline";
  children?: React.ReactNode;
}

export const FlexDiv: React.FC<FlexDivProps> = ({
  children,
  direction = "row",
  wrap = "wrap",
  gap = "0",
  alignContent = "start",
  alignItems = "start",
  justifyContent = "start",
  ...props
}) => {
  const classDirection = `flex-${direction}`;
  const classWrap = `flex-${wrap}`;
  const classGap = `gap-${gap}`;
  const classJustifyContent = `justify-content-${justifyContent}`;
  const classAlignItems = `align-items-${alignItems}`;
  const classAlignContent = `align-content${alignContent}`;

  return (
    <div
      className={`flex ${classDirection} ${classWrap} ${classGap} ${classJustifyContent} ${classAlignItems} ${classAlignContent} ${props.className}`}
      {...props}
    >
      {children}
    </div>
  );
};
