module.exports = {
  parser: "@typescript-eslint/parser",
  parserOptions: {
    project: "./tsconfig.json",
  },
  settings: {
    "import/parsers": {
      "@typescript-eslint/parser": [
        ".ts",
        ".tsx"
      ]
    },
    "import/resolver": {
      "typescript": {}
    },
  },
  ignorePatterns: [
    ".eslintrc.js",
    "jest.config.js",
    "next.config.js",
    "next-i18next.config.js"
  ],
  plugins: [
    "@typescript-eslint/eslint-plugin",
    "prettier",
    "simple-import-sort",
    // "import",
    "jest"
  ],
  extends: [
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:storybook/recommended",
    "airbnb-base",
    "airbnb-typescript/base",
    "prettier",
    "next"
  ],
  root: true,
  env: {
    node: true,
    jest: true,
  },
  rules: {
    "@typescript-eslint/explicit-function-return-type": "off",
    "@typescript-eslint/explicit-module-boundary-types": "off",
    "@typescript-eslint/no-explicit-any": "off",
    "import/prefer-default-export": "off",
    "import/extensions": "off",
    "prettier/prettier": "warn",
    "simple-import-sort/imports": "error",
    "no-restricted-imports": [
      "error",
      {
        "patterns": [
          {
            "group": [
              "@mui/material",
              "!@mui/material/"
            ],
            "message": "Use export for individual component. Example import Button from '@mui/material/Button';"
          },
          {
            "group": [
              "@mui/icons-material",
              "!@mui/icons-material/"
            ],
            "message": "Use export for individual icon. Example import CloseIcon from '@mui/icons-material/Close';"
          }
        ]
      }
    ],
    "@next/next/no-img-element": "off"
  },
  "overrides": [
    {
      // Stories will not be build for production build
      "files": [
        "src/stories/**/*.tsx"
      ],
      "rules": {
        "import/no-extraneous-dependencies": "off"
      }
    },
    {
      // We disable this, because variable self need to be reassign in actions.
      "files": [
        "src/models/**/*.mst.model.ts"
      ],
      "rules": {
        "no-param-reassign": "off"
      }
    }
  ]
};
