/* eslint-disable no-console */
import { Prisma, PrismaClient } from "@prisma/client";
import { genSaltSync, hashSync } from "bcrypt";

const prisma = new PrismaClient();

const salt = genSaltSync(10);

const userData: Prisma.UserCreateInput[] = [
  {
    name: "admin",
    username: "admin",
    email: "admin@none.none",
    password: hashSync("admin", salt),
    userPermision: {
      create: [{ permission: "SUPERUSER" }],
    },
  },
];

async function main() {
  console.log(`Start seeding ...`);
  // eslint-disable-next-line no-restricted-syntax
  for (const u of userData) {
    // eslint-disable-next-line no-await-in-loop
    const user = await prisma.user.create({
      data: u,
    });
    console.log(`Created user with id: ${user.id}`);
  }
  console.log(`Seeding finished.`);
}

main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
