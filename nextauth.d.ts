import { UserPermissionEnum } from "@prisma/client";
import { DefaultUser } from "next-auth";
// Define a role enum

// common interface for JWT and Session
interface IUser extends DefaultUser {
  permissions?: UserPermissionEnum[];
}
declare module "next-auth" {
  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  interface User extends IUser {}
  interface Session {
    user?: User;
  }
}
declare module "next-auth/jwt" {
  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  interface JWT {
    permissions?: UserPermissionEnum[];
  }
}
