# Xilel next,js auth bootplate

This Xilel next.js bootplate with nextauth, prisma, primereact, nodemailer. It will work with postregsql, mysql. For more check prisma.js

## Getting Started

### Configure .env

1. Create .env files.
1. Fill DATABASE_URL, NEXTAUTH_SECRET, NEXTAUTH_URL

### Migrate and seed database

1. Now run migrate database `npx prisma migrate dev --name init`
1. Then run seed process `npx prisma db seed`

### Now you can run dev server

1. To run dev server just write

``npm run dev``

2. Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.
- [Prisma.js](https://www.prisma.io/docs) - awesome JavaScript ORM library.
- [NextAuth.js](https://next-auth.js.org/getting-started/introduction) - Auth library for next.js
- [PrimeReact](https://www.primefaces.org/primereact-v8/) - good component lib for React
- [Notistack](https://notistack.com/) - lib for toast message
- [usehooks-ts](https://usehooks-ts.com/) - great lib of react hooks

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Reference

- [rest-nextjs-api-routes-auth](https://github.com/prisma/prisma-examples/tree/latest/typescript/rest-nextjs-api-routes-auth) - this project is based of this app
- [Next13 + NextAuth.js](https://codevoweb.com/setup-and-use-nextauth-in-nextjs-13-app-directory/) - good tutorial article about next-auth

## Authors

Created by Xilel, 2023

License MIT
